/*
1) Create a folder called s30
2) Make a file called index.js
3) Open your terminal to s30
4) Login to MongoDB Atlas
5) inside s30 npm init
*/


//7 Import express
const express = require('express')

const mongoose = require('mongoose');

//8 Create express app
const app = express();

//9 Create a const called PORT
const port = 3001;

//10Setup server to handle data from request
	//Allows our app to read JSON data
app.use(express.json());

	//Allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

//11 Add listen method for requests
//app.listen(port, ()=> console.log(`Server running at port ${port}`));


//12 Install mongoose - npm install mongoose
//13 Import mongoose
// const mongoose = require('mongoose');

//14 Go to mongodb atlas and change network access to 0.0.0.0

//15 Get connection string and change the password to your password (admin1234)
//mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//16 change myFirstDatabase to s30 - MongoDB will automatically create the database
//mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/s30?retryWrites=true&w=majority

//17 Connecting to MongoDB Atlas - add .connect() method
// mongoose.connect();

//18 Add the connection string as the 1st argument
//18b Add this object to allow connection
/*
		{
			useNewUrlParser: true,
			useUnifiedTopology: true	
		}
*/
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/s30?retryWrites=true&w=majority',
		{
			useNewUrlParser: true,
			useUnifiedTopology: true	
		}

	);

//19 Set notification for connection success or failure by using .connection property of mongoose
// mongoose.connection;

//20 store it in variable called db.
let db = mongoose.connection;

//21 console.error.bind(console) allow us to 
//print error in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection.error"));

//22 if the connection is successful, output this in the console
db.once("open", ()=> console.log("We're connected to the cloud database"))

//23 Schemas determine the structure of the documents to be written in the database
//Schemas act as blueprints to our data
const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding data type
	//For a task, it need a "task name" and "task status"
	//There is a field called "name" and its data type is "String"
	name: String,
	//There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});

//Models use Schemas and they act as the middleman from the server (JS code) to our database
//24 Create a model
//Models must be in a singular form and capitalized
//The first parameter of the Mongoose model method indicates the collection
//in where to store the data (tasks, plural form of the model)

//The second parameter is used to specify the Schema/blueprint
//of the documents that will be stored in the MongoDB collection

//Using Mongoose, the package was programmed well enough that it
//automatically converts the singular form of the model name
//into a plural form when creating a collection in postman

const Task = mongoose.model("Task", taskSchema)

/*
Businees Logic
1. Add a functionality to check if there are duplicate tasks
	-If the task already exist in the databaste, we return an error
	-If the task doesn't exist in the database, we add it in the database
*/

//25 Create our route
// app.post('/tasks'(req, res)=>{
// req.body.name;
// })

//26 Check if the task already exists 
//Use the Task model to interact with the tasks collection
app.post('/tasks', (req, res)=>{
  Task.findOne({name: req.body.name},(err, result)=> {
  	if(result != null && result.name == req.body.name){
  		//Return a message to the client/Postman
  		return res.send('Duplicate task found')
  	}else{
  		let newTask = new Task({
  			name: req.body.name
  		})

  		newTask.save((saveErr, savedTask)=>{
  			if(saveErr){
  				return console.error(saveErr)
  			}else{
  				return res.status(201).send('New task created')
  			}
  		})
  	}
  })
})


//27 Get all task
//See the contents of the task collection via the task model
//"find" is a mongoose method that is similar to Mongodb "find",  and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback functions
app.get('/tasks',(req, res)=>{
	Task.find({},(err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({data: result})
		}
	})
})



//Activity
//1. Create a User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});


//2. Make a model
const User = mongoose.model("User", userSchema)


//3. Register a User
app.post('/signup', (req, res)=>{
  User.findOne({username: req.body.username},(err, result)=> {
  	if(result != null && result.username == req.body.username){
  		//Return a message to the client/Postman
  		return res.send('Username is already available')
  	}else{
  		let newUser = new User({
  			username: req.body.username,
  			password: req.body.password
  		})

  		newUser.save((saveErr, savedUser)=>{
  			if(saveErr){
  				return console.error(saveErr)
  			}else{
  				return res.status(201).send('New Username created')
  			}
  		})
  	}
  })
})


//3a
app.get('/signup',(req, res)=>{
	User.find({},(err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({data: result})
		}
	})
})



app.listen(port, ()=> console.log(`Server running at port ${port}`));